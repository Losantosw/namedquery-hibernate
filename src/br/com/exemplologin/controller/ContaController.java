/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.exemplologin.controller;

import br.com.exemplologin.dao.ContaDAO;
import br.com.exemplologin.model.Conta;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.observablecollections.ObservableCollections;


public class ContaController {

    private ContaDAO dao;
    private List<Conta> contas;
    private Conta conta;

    public ContaController() {
        novo();
        dao = new ContaDAO();
        contas = ObservableCollections.observableList(new ArrayList<>());
        pesquisar();
    }

    public void pesquisar() {
        contas.clear();
        contas.addAll(dao.findContaEntities());
    }
    
    public void buscaContasPorMes(int mes){
        contas.clear();
        contas.addAll(dao.findContaByMonth(mes));
    }

    public void novo() {
        setConta(new Conta());
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public void salvarConta() {
        dao.create(conta);
        pesquisar();
        novo();
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }
}
